/*
 * psx_stdint.h
 */

#ifndef _PSX_STDINT_H_
#define _PSX_STDINT_H_

// We don't have a header for these with PSn00b at the moment
typedef          char  int8_t;
typedef unsigned char  uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int   uint32_t;

#endif

