/*
 * GameSelection.h
 */
#ifndef _GAME_SELECTION_H_INCLUDED_
#define _GAME_SELECTION_H_INCLUDED_

#include <sys/types.h>	// This provides typedefs needed by libgte.h and libgpu.h
#include <stdio.h>	// Not necessary but include it anyway
#include <psxetc.h>	// Includes some functions that controls the display
#include <psxgte.h>	// GTE header, not really used but libgpu.h depends on it
#include <psxgpu.h>	// GPU library header
#include <inline_c.h>

#include "psx_stdint.h"

// ROMs
#include "ROMs/A15PUZZLE.h"
#include "ROMs/BREAKOUT.h"
#include "ROMs/GUESS.h"
#include "ROMs/KALEID.h"
#include "ROMs/MISSILE.h"
#include "ROMs/PUZZLE.h"
#include "ROMs/TANK.h"
#include "ROMs/UFO.h"
#include "ROMs/WALL.h"
#include "ROMs/BLINKY.h"
#include "ROMs/BRIX.h"
#include "ROMs/HIDDEN.h"
#include "ROMs/MAZE.h"
#include "ROMs/PONG2.h"
#include "ROMs/SQUASH.h"
#include "ROMs/TETRIS.h"
#include "ROMs/VBRIX.h"
#include "ROMs/WIPEOFF.h"
#include "ROMs/BLITZ.h"
#include "ROMs/CONNECT4.h"
#include "ROMs/INVADERS.h"
#include "ROMs/MERLIN.h"
#include "ROMs/PONG.h"
#include "ROMs/SYZYGY.h"
#include "ROMs/TICTAC.h"
#include "ROMs/VERS.h"

/*
===============================================================================
GameSelection

    List of chip8 roms and selection logic
===============================================================================
*/
namespace GameSelection
{

typedef struct {
    uint8_t *rom;
    uint32_t rom_size;
    char *name;
} Chip8Game;

Chip8Game chip8Games[] = {
  { A15PUZZLE, A15PUZZLE_size, "15PUZZLE" },
  { BREAKOUT, BREAKOUT_size, "BREAKOUT" },
  { GUESS, GUESS_size, "GUESS" },
  { KALEID, KALEID_size, "KALEID" },
  { MISSILE, MISSILE_size, "MISSILE" },
  { PUZZLE, PUZZLE_size, "PUZZLE" },
  { TANK, TANK_size, "TANK" },
  { UFO, UFO_size, "UFO" },
  { WALL, WALL_size, "WALL" },
  { BLINKY, BLINKY_size, "BLINKY" },
  { BRIX, BRIX_size, "BRIX" },
  { HIDDEN, HIDDEN_size, "HIDDEN" },
  { MAZE, MAZE_size, "MAZE" },
  { PONG2, PONG2_size, "PONG2" },
  { SQUASH, SQUASH_size, "SQUASH" },
  { TETRIS, TETRIS_size, "TETRIS" },
  { VBRIX, VBRIX_size, "VBRIX" },
  { WIPEOFF, WIPEOFF_size, "WIPEOFF" },
  { BLITZ, BLITZ_size, "BLITZ" },
  { CONNECT4, CONNECT4_size, "CONNECT4" },
  { INVADERS, INVADERS_size, "INVADERS" },
  { MERLIN, MERLIN_size, "MERLIN" },
  { PONG, PONG_size, "PONG" },
  { SYZYGY, SYZYGY_size, "SYZYGY" },
  { TICTAC, TICTAC_size, "TICTAC" },
  { VERS, VERS_size, "VERS" }
};
uint8_t numGames = 25;
uint8_t gameIndex = 0;

/*  
===================
Draws the list of games and current selection;
To be called once per main application loop
===================
*/
void Run(void)
{
    static int offsetY = 20;

    POLY_F4 *hilightRect = (POLY_F4*)System::GetNextPrimitive();
    setPolyF4( hilightRect );
    setXY4( 
        hilightRect,
        5, gameIndex*10 + offsetY,       // X0, Y0 (Top Left)
        5+200, gameIndex*10 + offsetY,   // X1, Y1 (Top Right) 
        5, gameIndex*10+10 + offsetY,    // X2, Y2 (Bottom Left)
        5+200, gameIndex*10+10 + offsetY // X3, Y3 (Bottom Right)
    );
    setRGB0(
        hilightRect,
        0, 0, 255
    );

    // put at end of OT so its in the back
    // (OT is drawn in reverse order)
    System::AddPrimitive( OT_LEN-1, hilightRect );
    hilightRect++;
    System::SetNextPrimitive( (char*)hilightRect );

    // hilight the currently selected game
    // draw each game name
    for ( uint8_t i = 0; i < numGames; ++i )
    {
        // put at front of OT so its drawn last
        System::DrawText( 1, 10, i*10 + offsetY, chip8Games[i].name );
    }
}

/*  
===================
return the currently selected ROM memory pointer and size
===================
*/
uint8_t *GetRom(void)
{
    return chip8Games[gameIndex].rom;
}
uint32_t GetRomSize(void)
{
    return chip8Games[gameIndex].rom_size;
}

} // end namespace GameSelection

#endif // _GAME_SELECTION_H_INCLUDED_

