cmake_minimum_required(VERSION 2.6)

set(PROJ_NAME   "Chip8")
set(GCC_DIR     "/usr/local/mipsel-unknown-elf")
set(GCC_VERSION "7.2.0")
set(PSNOOB_DIR  "/home/bj/winmnt/Programming/psx/PSn00bSDK/libpsn00b")

set(SOURCES main.cpp System.cpp Chip8.cpp Pad.cpp)

set(CMAKE_SYSTEM_NAME Generic) # tell cmake we're cross compiling
set(CMAKE_CROSSCOMPILING 1)
set(CMAKE_C_COMPILER_WORKS 1)
set(CMAKE_CXX_COMPILER_WORKS 1)

set(CMAKE_C_COMPILER   "mipsel-unknown-elf-gcc")
set(CMAKE_CXX_COMPILER "mipsel-unknown-elf-g++")

project(${PROJ_NAME}.elf)

add_executable(${PROJ_NAME}.elf ${SOURCES})

include_directories( "${PSNOOB_DIR}/include"
                     "${GCC_DIR}/lib/gcc/mipsel-unknown-elf/${GCC_VERSION}/include")

link_directories( "${PSNOOB_DIR}/libpsn00b"
                  "${GCC_DIR}/lib/gcc/mipsel-unknown-elf/${GCC_VERSION}")

target_link_libraries(${PROJ_NAME}.elf psxetc psxgpu psxgte psxspu psxapi gcc c)

set(COMPILE_OPTIONS "-g -O2 -fno-builtin -fdata-sections -ffunction-sections")

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -g -Ttext=0x80010000 -gc-sections -T ${GCC_DIR}/mipsel-unknown-elf/lib/ldscripts/elf32elmip.x")

set(CMAKE_C_LINK_EXECUTABLE "mipsel-unknown-elf-ld ${CMAKE_EXE_LINKER_FLAGS} <OBJECTS> -L${GCC_DIR}/lib/gcc/mipsel-unknown-elf/${GCC_VERSION} -L${PSNOOB_DIR} <LINK_LIBRARIES> -o <TARGET>")
set(CMAKE_CXX_LINK_EXECUTABLE "mipsel-unknown-elf-ld ${CMAKE_EXE_LINKER_FLAGS} <OBJECTS> -L${GCC_DIR}/lib/gcc/mipsel-unknown-elf/${GCC_VERSION} -L${PSNOOB_DIR} <LINK_LIBRARIES> -o <TARGET>")

# elf2exe after build
add_custom_command(TARGET ${PROJ_NAME}.elf
                   POST_BUILD
                   COMMAND elf2x ARGS -q ${PROJ_NAME}.elf
                   COMMENT "elf2x -q ${PROJ_NAME}.elf")

