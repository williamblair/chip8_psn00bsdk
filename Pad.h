/*
 * Pad.h
 */

#ifndef _PAD_H_INCLUDED_
#define _PAD_H_INCLUDED_

#include <sys/types.h>	// This provides typedefs needed by libgte.h and libgpu.h
#include <stdio.h>	// Not necessary but include it anyway
#include <psxetc.h>	// Includes some functions that controls the display
#include <psxgte.h>	// GTE header, not really used but libgpu.h depends on it
#include <psxgpu.h>	// GPU library header
#include <psxpad.h> // Controller
#include <psxapi.h> // BIOs
#include <inline_c.h>

/*
===============================================================================
Pad

    Controller 1 and 2 handler
        TODO - controller 2
===============================================================================
*/
namespace Pad
{

/*  
===================
Init Pad reading and data
===================
*/
void Init(); 

/*  
===================
Update pad data
===================
*/
void Update(); 

/*  
===================
Returns true if given button currently held down
===================
*/
bool IsHeld( int button );

} // end namespace Pad

#endif // _PAD_H_INCLUDED_

