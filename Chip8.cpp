/*
 * Chip8.cpp
 */

#include "Chip8.h"

// Enable or disable debug printfs
#define DEBUG 0

#if DEBUG

#define DEBUG_PRINT(message, val) \
    printf((message), (val))

#else

#define DEBUG_PRINT(message, val)

#endif
    

namespace Chip8
{

/*
===================
returns the instruction based on the full opcode
===================
*/
#define GetInstructionFromOp(op) \
            ((op & 0xF000) >> 12)

/*
===================
returns the address for operations that use one (LSB 12 bits)
===================
*/
#define GetAddressFromOp(op) \
            (op & 0xFFF)

/*
===================
returns the first register for storage and cond (Bits 11:8)
===================
*/
#define GetRegisterXFromOp(op) \
            ((op & 0x0F00) >> 8)

/*
===================
returns the second register for storage and cond (Bits 7:4)
===================
*/
#define GetRegisterYFromOp(op) \
            ((op & 0x00F0) >> 4)

/*
===================
returns the immediate 8bit value from op (LSB 8 bits)
===================
*/
#define GetImmediateFromOp(op) \
            (op & 0xFF)

/*
===================
returns the first 4bit value from op (LSB 4 bits)
===================
*/
#define GetSubOperationFromOp(op) \
            (op & 0xF)


/*
===================
Get the value at the head of the stack and decrement the stack pointer
===================
*/
#define PopStack() \
            stack[--stackPointer]

/*
===================
Store the current PC value in the stack
===================
*/
#define PushStack() \
            stack[stackPointer++] = PC

/*
==================
Calculate the index for the display array based on x,y
==================
*/
#define GetDisplayIndex(row,col) \
            ((row)*64 + (col))

/*  
===================
System Data
===================
*/
uint8_t memory[0xFFF];  // system RAM
uint8_t registers[16];
uint8_t display[64*32]; // Video Memory
uint8_t keys[16];

uint8_t delayTimer;
uint8_t soundTimer;

uint16_t I;  // memory address register
uint16_t PC; // program counter
uint16_t fullOp; // All 16 bits of the current operation

uint16_t  stack[24];    // according to wiki, the original 1802 had 48 bytes of stack
uint8_t   stackPointer; // the current head of the stack

/*  
===================
Opcode lookup table
===================
*/
typedef void (*Chip8Op)(void);
Chip8Op opTable[16]; // 0..F

Chip8Op opTable_8[16]; // sub ops beginning with 8, not all 16 used
Chip8Op opTable_F[256]; // sub ops beginning with F, not all 255 used

/*
==================
Return a pixel value from the display array
==================
*/
uint8_t GetPixelValue(uint8_t row, uint8_t col)
{
    return display[GetDisplayIndex(row,col)];
}

void DecodeOp0(void)
{
    switch( GetImmediateFromOp(fullOp) )
    {
        // Clear the screen
        case 0xE0:
            memset(display, 0, sizeof(display));
            break;

        // Return from subroutine
        case 0xEE:
            PC = PopStack();

            if ((int8_t)stackPointer < 0) {
                printf("Invalid popstack! < 0!\n");
            }

            break;

        default:
            printf("Invalid 0 op: 0x%X\n", fullOp);
            break;
    }
}
void DecodeOp8(void)
{
    opTable_8[ GetSubOperationFromOp(fullOp) ]();
}
void DecodeOpE(void)
{
    switch ( GetSubOperationFromOp(fullOp) )
    {
        // skip the next op if key from register X is pressed
        case 14:
        {
            uint8_t regX = GetRegisterXFromOp( fullOp );
            uint8_t key = registers[regX];

            if ( key > 15 ) {
                printf("  KEY - INVALID KEY INDEX!\n");
            }

            if ( keys[key] ) 
            {
                PC += 2;
            }

            break;
        }

        // skip the next op if key from register X is NOT pressed
        case 1:
        {
            uint8_t regX = GetRegisterXFromOp( fullOp );
            uint8_t key = registers[regX];

            if ( key > 15 ) {
                printf("  KEY - INVALID KEY INDEX!\n");
            }

            if ( !keys[key] ) 
            {
                PC += 2;
            }
            
            break;
        }

        default:
            printf("Invalid sub E op: 0x%X\n", fullOp );
            break;
    }
}
void DecodeOpF(void)
{
    DEBUG_PRINT("In decode op %s!\n", "F");
    DEBUG_PRINT("Get immediate: 0x%X\n", GetImmediateFromOp(fullOp));

    opTable_F[ GetImmediateFromOp(fullOp) ]();
}

/*
===================
Opcodes
===================
*/

void Op00E0(void)
{
    memset(display, 0, sizeof(display));
}
void Op00EE(void)
{
    PC = PopStack();

    if ((int8_t)stackPointer < 0) {
        printf("Invalid popstack! < 0!\n");
    }
}
void Op1NNN(void)
{
    PC = GetAddressFromOp( fullOp );

    DEBUG_PRINT("op 1, pc: 0x%04X\n", PC);
}
void Op2NNN(void)
{
    PushStack();
    PC = GetAddressFromOp( fullOp );

    DEBUG_PRINT("op 2, stack val: 0x%04X\n", stack[stackPointer-1]);
    DEBUG_PRINT("  op 2, PC: 0x%04X\n", PC);
}
void Op3XNN(void)
{
    uint8_t reg = GetRegisterXFromOp( fullOp );
    if ( registers[reg] == GetImmediateFromOp(fullOp) ) 
    {
        PC += 2;
    }

    DEBUG_PRINT("op 3, reg: 0x%X\n", reg);
    DEBUG_PRINT("op 3, regval: 0x%X\n", registers[reg]);
    DEBUG_PRINT("op 3, imm: 0x%X\n", GetImmediateFromOp(fullOp));
    DEBUG_PRINT("op 3, PC: 0x%X\n", PC);
}
void Op4XNN(void)
{

    uint8_t reg = GetRegisterXFromOp( fullOp );
    if ( registers[reg] != GetImmediateFromOp(fullOp) ) 
    {
        PC += 2;
    }
}
void Op5XY0(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    if ( registers[regX] == registers[regY] )
    {
        PC += 2;
    }
}
void Op6XNN(void)
{
    uint8_t regX    = GetRegisterXFromOp( fullOp );
    registers[regX] = GetImmediateFromOp( fullOp );

    DEBUG_PRINT("op 6, register: 0x%X\n", regX);
    DEBUG_PRINT("op 6, reg val: 0x%02X\n", registers[regX]);
}
void Op7XNN(void)
{
    uint8_t   regX   = GetRegisterXFromOp( fullOp );
    registers[regX] += GetImmediateFromOp( fullOp );

    DEBUG_PRINT("op 7, register: 0x%X\n", regX);
    DEBUG_PRINT("op 7, reg val: 0x%02X\n", registers[regX]);

}
void Op8XY0(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    registers[regX] = registers[regY];
}
void Op8XY1(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    registers[regX] |= registers[regY];
    
}
void Op8XY2(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    registers[regX] &= registers[regY];
}
void Op8XY3(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    registers[regX] ^= registers[regY];
}
void Op8XY4(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    uint16_t result = registers[regX] + registers[regY];

    registers[0xF]  = (result > 0xFF);
   // registers[regX] = (uint8_t)result;
    registers[regX] += registers[regY];
}
void Op8XY5(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    // this flag set is opposite of above: 1 if NO underflow, else 0
    registers[0xF]  = !(registers[regX] < registers[regY]);
    registers[regX] -= registers[regY];

}
void Op8XY6(void)
{

    uint8_t regX = GetRegisterXFromOp( fullOp );

    registers[0xF] = registers[regX] & 1;

    registers[regX] >>= 1;

}
void Op8XY7(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    registers[0xF] = !(registers[regY] < registers[regX]);
    registers[regX] = registers[regY] - registers[regX];

}
void Op8XYE(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );

    registers[0xF] = registers[regX] >> 7;
    registers[regX] <<= 1;
}
void Op9XY0(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    if ( registers[regX] != registers[regY] )
    {
        PC += 2;
    }
}
void OpANNN(void)
{
    I = GetAddressFromOp( fullOp );
    
}
void OpBNNN(void)
{
    PC = GetAddressFromOp( fullOp ) + registers[0];

}
void OpCXNN(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );

    registers[regX] = rand() & GetImmediateFromOp( fullOp );

}
void OpDXYN(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t regY = GetRegisterYFromOp( fullOp );

    uint8_t coordX = registers[regX];
    uint8_t coordY = registers[regY];

    uint8_t height = GetSubOperationFromOp( fullOp );

    // by default, the flag is set to 0 for no 'collision'
    registers[0xF] = 0;

    for ( int row = 0; row < height; ++row )
    {
        // get the sprite row pixels
        uint8_t rowData = memory[I + row];

        // draw each pixel, starting at the far right (LSB of row data)
        for ( int col = 7; col >= 0; --col )
        {

            // extract the current bit from the row
            uint8_t bit = rowData & 1;

            // if the sprite bit is set (not transparent, we draw)
            if ( bit )
            {
                // if the screen memory already has a pixel there (collision), set the flag
                if ( display[GetDisplayIndex(coordY+row, coordX+col)] ) 
                {
                    registers[0xF] = 1;
                    display[GetDisplayIndex(coordY+row, coordX+col)] = 0;
                }
                else {
                    display[GetDisplayIndex(coordY+row, coordX+col)] = 1;
                }
            }

            // move to the next bit in the row data
            rowData >>= 1;

        } // end for each col
    } // end for each row 
}
void OpEX9E(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t key = registers[regX];

    if ( key > 15 ) {
        printf("  KEY - INVALID KEY INDEX!\n");
    }

    if ( keys[key] ) 
    {
        PC += 2;
    }
}
void OpEXA1(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t key = registers[regX];

    if ( key > 15 ) {
        printf("  KEY - INVALID KEY INDEX!\n");
    }

    if ( !keys[key] ) 
    {
        PC += 2;
    }
}
void OpFX07(void)
{

    uint8_t regX = GetRegisterXFromOp( fullOp );

    registers[regX] = delayTimer;
}
void OpFX0A(void)
{
    // TODO - maybe set like a system boolean or something
    printf("\n  UNIMPLEMENTED WAIT FOR KEY PRESS!\n");
}
void OpFX15(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    delayTimer = registers[regX];

}
void OpFX18(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    soundTimer = registers[regX];

}
void OpFX1E(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    I += registers[regX];

}
void OpFX29(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    
    // copied from codeslinger chip8 emulator
    I = registers[regX]*5;
}
void OpFX33(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    uint8_t val = registers[regX];

    // copied from codeslinger chip8 emulator
    uint8_t hundreds = val / 100;
    uint8_t tens = (val / 10) % 10;
    uint8_t ones = val % 10;

    memory[I+0] = hundreds;
    memory[I+1] = tens;
    memory[I+2] = ones;

}
void OpFX55(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    
    for ( int i = 0; i <= regX; ++i )
    {
        memory[I+i] = registers[i];
    }
    
    // codeslinger updates I here, but wikipedia says I is left unmodified...
    //I += regX+1;
}
void OpFX65(void)
{
    uint8_t regX = GetRegisterXFromOp( fullOp );
    
    for ( int i = 0; i <= regX; ++i )
    {
        registers[i] = memory[I+i];
    }
    
    // codeslinger updates I here, but wikipedia says I is left unmodified...
    //I += regX+1;
}



/*  
===================
Initialize Memory
===================
*/
void Init( uint8_t *rom, uint32_t rom_size )
{
    memset(memory,    0, sizeof(memory));
    memset(registers, 0, sizeof(registers));
    memset(keys,      0, sizeof(keys));
    memset(display,   0, sizeof(display));

    delayTimer = 0;
    soundTimer = 0;
    
    I  = 0;
    PC = 0x200;

    memset(stack, 0, sizeof(stack));
    stackPointer = 0;

    memcpy(&memory[0x200], rom, rom_size);
    
    // Set opcode function pointers
    opTable[0x0] = DecodeOp0;
    opTable[0x1] = Op1NNN;
    opTable[0x2] = Op2NNN;
    opTable[0x3] = Op3XNN;
    opTable[0x4] = Op4XNN;
    opTable[0x5] = Op5XY0;
    opTable[0x6] = Op6XNN;
    opTable[0x7] = Op7XNN;
    opTable[0x8] = DecodeOp8;
    opTable[0x9] = Op9XY0;
    opTable[0xA] = OpANNN;
    opTable[0xB] = OpBNNN;
    opTable[0xC] = OpCXNN;
    opTable[0xD] = OpDXYN;
    opTable[0xE] = DecodeOpE;
    opTable[0xF] = DecodeOpF;

    opTable_8[0x0] = Op8XY0;
    opTable_8[0x1] = Op8XY1;
    opTable_8[0x2] = Op8XY2;
    opTable_8[0x3] = Op8XY3;
    opTable_8[0x4] = Op8XY4;
    opTable_8[0x5] = Op8XY5;
    opTable_8[0x6] = Op8XY6;
    opTable_8[0x7] = Op8XY7;
    opTable_8[0xE] = Op8XYE;

    opTable_F[0x07] = OpFX07;
    opTable_F[0x0A] = OpFX0A;
    opTable_F[0x15] = OpFX15;
    opTable_F[0x18] = OpFX18;
    opTable_F[0x1E] = OpFX1E;
    opTable_F[0x29] = OpFX29;
    opTable_F[0x33] = OpFX33;
    opTable_F[0x55] = OpFX55;
    opTable_F[0x65] = OpFX65;
    
}

/*
===================
Gets the next operation from memory based on PC and updates PC
===================
*/
uint16_t GetNextOp(void)
{
    uint16_t op = (memory[PC] << 8) |
                  memory[PC+1];

    PC += 2;
    
    return op;
}

/*
===================
Decrement timers if > 0
===================
*/
void UpdateTimers()
{
    if ( delayTimer > 0 ) --delayTimer;
    if ( soundTimer > 0 ) --soundTimer;

    // TODO - beep noise if soundTimer > 0
}

/*
===================
Runs the appropriate op based on GetNextOp
    calls GetNextOp internally
===================
*/
void RunNextOp(void)
{
    fullOp = GetNextOp();

    DEBUG_PRINT("Opcode: 0x%04X\n", fullOp);

    opTable[ GetInstructionFromOp(fullOp) ]();
}

} // end namespace Chip8



