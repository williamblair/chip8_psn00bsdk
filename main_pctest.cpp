/*
 * Chip8 Emulator
 */

#include <cstdio>

#include "Chip8.h"

#define ever ;;

void drawChip8Screen(void);


int main(void) 
{
    Chip8::Init();


    for(ever)
    {
        getchar();

        Chip8::RunNextOp();

        drawChip8Screen();
    }

    return 0;	
}

void drawChip8Screen(void)
{
#if 0
    // width and height of each pixel
    static uint8_t width =4;
    static uint8_t height=4;

    static uint8_t offsetX = 0;
    static uint8_t offsetY = 10;

    //const uint8_t *display = Chip8::display;

    POLY_F4 *p4 = (POLY_F4 *)System::GetNextPrimitive();


    for (int row=0; row<32; ++row)
    {
        for (int col=0; col<64; ++col)
        {
            setPolyF4(p4);
            
            // top left
            p4->x3 = col*width + offsetX;
            p4->y3 = row*height + offsetY;
            // top right
            p4->x2 = col*width+width + offsetX;
            p4->y2 = row*height + offsetY;
            // bottom right
            p4->x1 = col*width+width + offsetX;
            p4->y1 = row*height+height + offsetY;
            // bottom left
            p4->x0 = col*width + offsetX;
            p4->y0 = row*height+height + offsetY;

            uint8_t color;
            //if (Chip8::display[row*64+col] == 1) {
            if (Chip8::GetPixelValue(row,col) == 1) {
                color = 255;
            }
            else {
                color = 0;
            }

            p4->r0 = color;
            p4->g0 = color;
            p4->b0 = color;

            // position in order table arbitrary
            System::AddPrimitive(OT_LEN-1, p4);

            p4++;
        }
    }

    System::SetNextPrimitive((char*)p4);
#endif

    for ( int row = 0; row < 32; ++row)
    {
        for ( int col = 0; col < 64; ++col )
        {
            printf("%d", Chip8::GetPixelValue(row,col));
        }

        printf("\n");
    }

}

