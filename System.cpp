/*
 * System.cpp
 */

#include "System.h"

namespace System
{

typedef struct DB {
    
    DISPENV disp;
    DRAWENV draw;

    unsigned int ot[OT_LEN];     // order table
    char p[PACKET_LEN]; // packet buffer

} DB;

DB  db[2];
int db_active = 0;
char *db_nextpri; // next primitive

// screen size
#define S_WIDTH  320
#define S_HEIGHT 240


/*  
===================
ResetGraph, initialize display and draw envs
===================
*/
void Init()
{

    // Reset graphics
    ResetGraph(0);

    // First buffer
    SetDefDispEnv( &db[0].disp, 0, 0, S_WIDTH, S_HEIGHT );
    SetDefDrawEnv( &db[0].draw, 0, S_HEIGHT, S_WIDTH, S_HEIGHT );
    
    // Second buffer
    SetDefDispEnv( &db[1].disp, 0, S_HEIGHT, S_WIDTH, S_HEIGHT );
    SetDefDrawEnv( &db[1].draw, 0, 0, S_WIDTH, S_HEIGHT );

    db[0].draw.isbg = 1;               // Enable clear
    setRGB0( &db[0].draw, 63, 0, 127 );  // Set clear color
    // db[0].draw.dtd = 1; // enable dither processing
    
    db[1].draw.isbg = 1;
    setRGB0( &db[1].draw, 63, 0, 127 );
    // db[1].draw.dtd = 1; // enable dither processing

    // apply the first drawing env
    PutDrawEnv( &db[0].draw );

    // clear order tables
    ClearOTagR( db[0].ot, OT_LEN );
    ClearOTagR( db[1].ot, OT_LEN );

    // set primitive pointer address
    db_nextpri = db[0].p;

    // init the gte
    InitGeom();

    // set GTE offset (recommended method of centering)
    // set to center of screen (width/2, height/2);
    gte_SetGeomOffset( S_WIDTH >> 1 , S_HEIGHT >> 1 );

    // set screen depth (FOV control, width/2 works best)
    gte_SetGeomScreen( S_WIDTH >> 1 );

    // Load the BIOS font to memory
    FntLoad(960, 256); // load BIOS font into VRAM location 768,256
}

void Display() 
{
    DrawSync(0);
    VSync(0);               // Wait for vertical retrace

    db_active = !db_active;               // Swap buffers on every pass (alternates between 1 and 0)
    db_nextpri = db[db_active].p;         // reset the primitive ot pointer

    ClearOTagR( db[db_active].ot, OT_LEN );

    PutDispEnv(&db[db_active].disp);  // Apply the DISPENV/DRAWENVs
    PutDrawEnv(&db[db_active].draw);

    SetDispMask(1);         // Enable the display

    DrawOTag( db[1-db_active].ot+(OT_LEN-1) ); // start drawing the OT of the LAST buffer 
}

void AddPrimitive(int offset, void *prim)
{
//    printf("ot offset: %d\n", offset);
//    printf("Prim: 0x%X\n", prim);
    addPrim( db[db_active].ot+(offset), prim );
}

void DrawText(int offset, int x, int y, const char *message)
{
    // Might need to edit OT for offset...
    db_nextpri = FntSort( &db[db_active].ot[offset], db_nextpri, x, y, message );
}

char *GetNextPrimitive(void)
{
    return db_nextpri;
}

void SetNextPrimitive(char *prim)
{
    db_nextpri = prim;
}

} // end namespace system

