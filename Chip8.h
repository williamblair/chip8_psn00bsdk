/*
 * Chip8.h
 */

#ifndef _CHIP8_H_INCLUDED_
#define _CHIP8_H_INCLUDED_

#include <sys/types.h>	// This provides typedefs needed by libgte.h and libgpu.h
#include <stdio.h>	// Not necessary but include it anyway
#include <string.h>
#include <stdlib.h>

#include <psxetc.h>	// Includes some functions that controls the display
#include <psxgte.h>	// GTE header, not really used but libgpu.h depends on it
#include <psxgpu.h>	// GPU library header
#include <inline_c.h>

#include "psx_stdint.h"

/*
===============================================================================
Chip8

    Data and functions for the Chip8 system
===============================================================================
*/
namespace Chip8
{

extern uint8_t display[64*32]; // Video Memory
extern uint8_t keys[16];       // key input

/*  
===================
Initialize Memory, Copies ROM into memory
===================
*/
void Init( uint8_t *rom, uint32_t rom_size );

/*
===================
Gets the next operation from memory based on PC and updates PC
===================
*/
uint16_t GetNextOp(void);

/*
===================
Runs the appropriate op based on GetNextOp
    calls GetNextOp internally
===================
*/
void RunNextOp(void);

/*
===================
Return a pixel value from the display array
===================
*/
uint8_t GetPixelValue(uint8_t row, uint8_t col);

/*
===================
Decrement timers if > 0
===================
*/
void UpdateTimers(void);

} // end namespace Chip8

#endif // _Chip8_H_INCLUDED_

