/*
 * System.h
 */

#ifndef _SYSTEM_H_INCLUDED_
#define _SYSTEM_H_INCLUDED_

#include <sys/types.h>	// This provides typedefs needed by libgte.h and libgpu.h
#include <stdio.h>	// Not necessary but include it anyway
#include <psxetc.h>	// Includes some functions that controls the display
#include <psxgte.h>	// GTE header, not really used but libgpu.h depends on it
#include <psxgpu.h>	// GPU library header
#include <inline_c.h>

// order table and packet lengths
#define OT_LEN     4096
//#define PACKET_LEN (8192*8)
#define PACKET_LEN 49153 // THIS IS SPECIFIC TO Chip8 (64*32*sizeof(POLY_F4))
//#define OT_LEN     4096
//#define PACKET_LEN (8192*2)


/*
===============================================================================
System

    PSX initialization and display/draw environments wrapper
===============================================================================
*/
namespace System
{

/*  
===================
ResetGraph, initialize display and draw envs
===================
*/
void Init(); // TODO - add bg color

/*
===================
VSyncs, sets and swaps display buffers 
===================
*/
void Display();

/*
===================
Add a primitive to be drawn to the ot
===================
*/
void AddPrimitive(int offset, void *prim);

/*
===================
Render message at given location using bios font
(assumes FntLoad() has already been called)
===================
*/
void DrawText(int offset, int x, int y, const char *message);

/*
===================
Get and Set next primitive pointer for the order table
===================
*/
char *GetNextPrimitive(void);
void SetNextPrimitive( char *prim );

} // end namespace system

#endif // _SYSTEM_H_INCLUDED_

