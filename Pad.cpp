/*
 * Pad.cpp
 */

#include "Pad.h"

namespace Pad
{

#define BUTTON_PRESSED(button) \
    (!(pad->btn & button))
   

char pad_buff[2][34];
PADTYPE *pad;

/*  
===================
Init Pad reading and data
===================
*/
void Init()
{
    // Init BIOS pad driver and set pad buffers (buffers are updated
    // automatically on every V-Blank)
    _InitPad(&pad_buff[0][0], 34, &pad_buff[1][0], 34);
 
    // Start pad
    _StartPad();

    // Don't make pad driver acknowledge V-Blank IRQ (recommended)
    ChangeClearPAD(0);
}

/*
===================
Update pad data
===================
*/
void Update()
{
   pad = (PADTYPE*)&pad_buff[0][0]; // assuming first index 0 is controller port 0
}

/*  
===================
Returns true if given button currently held down
===================
*/
bool IsHeld( int button )
{
    bool status = false;
    
    if ( pad->stat == 0 )
    {
        // digital, dual-analog, and dual shock
        if ( pad->type == 0x4 || pad->type == 0x5 || pad->type == 0x7 )
        {
            status = BUTTON_PRESSED( button );
        }
    }

    return status;
}

} // end namespace Pad

