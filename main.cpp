/*
 * Chip8 Emulator
 */

#include <stdio.h>
#include <psxgpu.h>
#include <psxgte.h>
#include <psxpad.h>
#include <psxapi.h>
#include <inline_c.h>

#include "System.h"
#include "Chip8.h"
#include "Pad.h"

#include "psx_stdint.h"

#include "GameSelection.h"

#define ever ;;

static inline void drawChip8Screen(void);

int pad_pressed = 0;

uint16_t screenClut[256]; // black, white

POLY_FT4 screenSpr;

RECT imgRect =  { 320, 0,   64>>1, 32 }; // x,y,w,h, divide by 2 since 8bit
RECT clutRect = { 320, 256, 256,  1  };

int main(void) 
{
    System::Init();
    Pad::Init();

    // clut index zero is black (transparent), everything else is white
    screenClut[0] = 0x0000;
    memset( &screenClut[1], 0xFF, sizeof(screenClut) - 1 );

    // load to VRAM
    LoadImage( &clutRect, (unsigned int *)screenClut );

    // initialize screen sprite
    setPolyFT4( &screenSpr );
    
    setXY4( 
        &screenSpr, 
        10, 10,     // X0, Y0 (top left)
        310, 10,   // X1, Y1 (top right)
        10, 230,   // X2, Y2 (bottom left)
        310, 230  // X3, Y3 (bottom right)
    );

    setUV4(
        &screenSpr,
        0, 0,       // U0, V0 (top left)
        64, 0,      // U1, V1 (top right)
        0, 32,      // U2, V2 (bottom left)
        64, 32      // U3, V3 (bottom right)
    );

    setRGB0( &screenSpr, 127, 127, 127 ); // unmodified image color

    // set texturepage
    setTPage( 
        &screenSpr,
        1, // texture page mode, 0 - 4bit, 1 - 8bit, 2 - 16bit
        0, // semitransparency rate
        320, 0  // X, Y in VRAM
    );

    // set clut
    setClut( &screenSpr, 320, 256 );

    static int emuSpeed=8; // how many ops to run per cycle
    static int delaySpeed=1; // how many times to decrement the delay per cycle

    // select which rom to run
    bool selected = false;
    do
    {
        Pad::Update();
        if ( Pad::IsHeld(PAD_CROSS) ) {
            printf("Selection!\n");
            selected = true;
        }
        else if ( Pad::IsHeld(PAD_UP) ) {
            if ( GameSelection::gameIndex > 0 ) {
                GameSelection::gameIndex--;
            }
        }
        else if ( Pad::IsHeld(PAD_DOWN ) ) {
            if ( GameSelection::gameIndex < GameSelection::numGames-1 ) {
                GameSelection::gameIndex++;
            }
        }

        GameSelection::Run();
        
        System::Display();
        
    } while( !selected );

    // init chip8 with the selected rom
    Chip8::Init( GameSelection::GetRom(), GameSelection::GetRomSize() );

    // run the rom
    for(ever)
    {
        Pad::Update();

        for ( int i = 0; i < emuSpeed; ++ i ) {
            Chip8::RunNextOp();
        }

        // keys should default to 0 each frame
        Chip8::keys[0x4] = 0;
        Chip8::keys[0x5] = 0;
        Chip8::keys[0x6] = 0;

        if ( Pad::IsHeld(PAD_LEFT) )  Chip8::keys[0x4] = 1;
        if ( Pad::IsHeld(PAD_RIGHT) ) Chip8::keys[0x6] = 1;
        if ( Pad::IsHeld(PAD_CROSS) ) Chip8::keys[0x5] = 1;

        drawChip8Screen();
        System::AddPrimitive( 1, &screenSpr );

        for ( int i = 0; i < delaySpeed; ++i ) {
            Chip8::UpdateTimers();
        }

        System::Display();
    }

    return 0;	
}

static inline void drawChip8Screen(void)
{
    LoadImage(&imgRect, (unsigned int *)Chip8::display);
}


